// Worst FizzBuzz ever
//
// P.S.: Using C++20 concepts should be even worse maybe?..
// TODO: Investigate.
//
#include <iostream>
#include <string>
using namespace std;

template<int n> string fz3()
{
   return (n%5) ? "fizz" : "fizzbuzz";
}

template<int n> string fz()
{
   if(!(n%3))
      return fz3<n>(); // Saving a step
   else if(!(n%5))
      return "buzz";
   return "";
}

template<int n> struct Fb
{
   // Here's the most delicious part, because it's really
   // just the typical 'factorial' with some rt concatenations.
   const string value = Fb<n-1>{}.value + "\n" + to_string(n) + ": " + fz<n>();
};

template<> struct Fb<1>
{
   const string value = "1:";
};


int main()
{
   Fb<42> fb;
   cout<<fb.value<<endl;
   return 0;
}
